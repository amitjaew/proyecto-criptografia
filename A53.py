from utils.sboxes import S9, S7
# Adaptation of python2 Kasumi Algorithm: https://github.com/bozhu/KASUMI-Python


def assert_length(n, max_n):
    assert (n >= 0) and ( (len(bin(n)) - 2) <= max_n )


def shift_16(n, k):
    assert_length(n, 16)
    return ((n << k) & 0xFFFF ) | (n >> (16 - k))


def mod_8(n):
    return ((n - 1) % 8) + 1


class A5_3:
    def __init__(self, init_key):
        assert_length(init_key, 128)
        self.KL_1       =   [None for i in range(9)]
        self.KL_2       =   [None for i in range(9)]
        self.KO_1       =   [None for i in range(9)]
        self.KO_2       =   [None for i in range(9)]
        self.KO_3       =   [None for i in range(9)]
        self.KI_1       =   [None for i in range(9)]
        self.KI_1       =   [None for i in range(9)]
        self.KI_2       =   [None for i in range(9)]
        self.KI_3       =   [None for i in range(9)]
        base_key        =   [None for i in range(9)]
        base_key_prime  =   [None for i in range(9)]

        init_key_prime = init_key ^ 0x0123456789ABCDEFFEDCBA9876543210
        for i in range(1,9):
            base_key[i]         = (init_key         >> (16 * (8-i))) & 0xFFFF
            base_key_prime[i]   = (init_key_prime   >> (16 * (8-i))) & 0xFFFF

        for i in range(1,9):
            self.KL_1[i] = shift_16(base_key[mod_8(i + 0)], 1)
            self.KL_2[i] = base_key_prime[mod_8(i + 2)]
            self.KO_1[i] = shift_16(base_key[mod_8(i + 1)], 5)
            self.KO_2[i] = shift_16(base_key[mod_8(i + 5)], 8)
            self.KO_3[i] = shift_16(base_key[mod_8(i + 6)], 13)
            self.KI_1[i] = base_key_prime[mod_8(i + 4)]
            self.KI_2[i] = base_key_prime[mod_8(i + 3)]
            self.KI_3[i] = base_key_prime[mod_8(i + 7)]

    def FI(self, input, round_key):
        # Left and Right side operations on Feistel-like Network
        l = input >> 7
        r = input & 127                 # 2^7 - 1 = 127 (mask for first 7 bits)

        round_key_1 = round_key >> 9
        round_key_2 = round_key & 511   # 2^9 - 1 = 511 (mask for first 9 bits)

        temp_l = r
        temp_r = S9[l] ^ r

        l = temp_r ^ round_key_2
        r = S7[temp_l] ^ (temp_r & 127) ^ round_key_1

        temp_l = r
        temp_r = S9[l] ^ r

        l = S7[temp_l] ^ (temp_r & 127)
        r = temp_r
        return (l << 9) | r

    def FO(self, input, round_iter):
        # Left and Right side operations on Feistel-like Network
        in_l = input >> 16
        in_r = input & 0xFFFF           # (mask for first 16 bits)

        out_l = in_r
        out_r = self.FI(
            in_l ^ self.KO_1[round_iter],
            in_r ^ self.KI_1[round_iter]
        )

        in_l = out_r
        in_r = self.FI(
            out_l ^ self.KO_2[round_iter],
            out_r ^ self.KI_2[round_iter]
        )

        out_l = in_r
        out_r = self.FI(
            in_l ^ self.KO_3[round_iter],
            in_r ^ self.KI_3[round_iter]
        )
        return (out_l << 16) | out_r

    def FL(self, input, round_iter):
        in_l = input >> 16
        in_r = input & 0xFFFF           # (mask for first 16 bits)

        out_r = in_r ^ shift_16(in_l & self.KL_1[round_iter], 1)
        out_l = in_l ^ shift_16(out_r | self.KL_2[round_iter], 1)
        return (out_l << 16) | out_r

    def F(self, input, round_iter):
        if (round_iter % 2 == 0):
            temp = self.FO(input, round_iter)
            output = self.FL(temp, round_iter)
        else:
            temp = self.FL(input, round_iter)
            output = self.FO(input, round_iter)
        return output

    def encryption_round(self, in_l, in_r, round_iter):
        out_r = in_l
        out_l = in_r ^ self.F(in_l, round_iter)
        return out_l, out_r

    def decryption_round(self, in_l, in_r, round_iter):
        out_l = in_r
        out_r = in_l ^ self.F(in_r, round_iter)
        return out_l, out_r

    def encrypt(self, msg):
        assert_length(msg, 64)
        l = msg >> 32
        r = msg & 0xFFFFFFFF
        for i in range(1,9):
            l, r = self.encryption_round(l, r, i)

        return (l << 32) | r

    def decrypt(self, encrypted):
        assert_length(encrypted, 64)
        l = encrypted >> 32
        r = encrypted & 0xFFFFFFFF
        for i in range(8, 0, -1):
            l, r = self.decryption_round(l, r, i)
        return (l << 32) | r


if __name__ == '__main__':
    key     = 0x9900aabbccddeeff1122334455667788
    msg     = 0xabcd1234abcd4321
    cypher = A5_3(key)

    enc = cypher.encrypt(msg)
    dec = cypher.decrypt(enc)

    print(f'Message:  \t {hex(msg)}')
    print(f'Cypher:   \t {hex(enc)}')
    print(f'Decrypted:\t {hex(dec)}')
