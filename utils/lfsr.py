# 64 bit A5/1
# (0 1 2 5 19)
def shift_19(n):
    msb = (n ^ (n >> 1) ^ (n >> 2) ^ (n >> 5)) & 1
    return (n >> 1) | (msb << 18)


# (0 1 22)
def shift_22(n):
    msb = (n ^ (n >> 1)) & 1
    return (n >> 1) | (msb << 21)


# (0 5 23)
def shift_23(n):
    msb = (n ^ (n >> 5)) & 1
    return (n >> 1) | (msb << 22)


# 32 bit A5/1
# (0 1 9)
def shift_9(n):
    msb = (n ^ (n >> 1)) & 1
    return (n >> 1) | (msb << 8)


# (0 2 11)
def shift_11(n):
    msb = (n ^ (n >> 2)) & 1
    return (n >> 1) | (msb << 10)


# (0 3 12)
def shift_12(n):
    msb = (n ^ (n >> 3)) & 1
    return (n >> 1) | (msb << 11)
