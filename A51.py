from utils.lfsr import shift_19, shift_22, shift_23


# A5 Cypher demo with public key initialized at 0
class A5_1:
    def __init__(self, priv_key):
        key = priv_key
        self.key = key
        self.register_1 = key & ((1 << 19) - 1)
        self.register_2 = (key >> 19) & ((1 << 22) - 1)
        self.register_3 = (key >> 41) & ((1 << 23) - 1)
        self.active_1 = False
        self.active_2 = False
        self.active_3 = False
        self.setup_keys()

    def setup_keys(self):
        for i in range(100):
            self.clock()

        key = 0b0
        for i in range(114):
            key = (self.clock() << i) | key
        self.keystream_1 = key

        for i in range(100):
            self.clock()

        key = 0b0
        for i in range(114):
            key = (self.clock() << i) | key
        self.keystream_2 = key

    def get_keystreams(self):
        return (
            self.keystream_1,
            self.keystream_2
        )

    # tau values: (9, 10, 11)
    def majority_function(self):
        m_1 = (self.register_1 >> 8) & 1
        m_2 = (self.register_2 >> 9) & 1
        m_3 = (self.register_3 >> 10) & 1
        if (m_1 == m_2 == m_3):
            self.active_1, self.active_2, self.active_3 = True, True, True
        elif (m_1 == m_2):
            self.active_1, self.active_2 = True, True
        elif (m_1 == m_3):
            self.active_1, self.active_3 = True, True
        elif (m_2 == m_3):
            self.active_2, self.active_3 = True, True

    def clock(self):
        self.majority_function()
        if (self.active_1):
            self.register_1 = shift_19(self.register_1)
        if (self.active_2):
            self.register_2 = shift_22(self.register_2)
        if (self.active_3):
            self.register_3 = shift_23(self.register_3)

        lsb_1 = self.register_1 & 1
        lsb_2 = self.register_2 & 1
        lsb_3 = self.register_3 & 1
        return lsb_1 ^ lsb_2 ^ lsb_3

if (__name__ == '__main__'):
    msg     = 0xabcd1234
    key     = 0x1a3b
    cypher = A5_1(key)
    keystream_1, keystream_2 = cypher.get_keystreams()

    enc = msg ^ keystream_1
    dec = enc ^ keystream_1

    print(f'Message:  \t {hex(msg)}')
    print(f'Cypher:   \t {hex(enc)}')
    print(f'Decrypted:\t {hex(dec)}')

